var obj = [{name:"company",type:"IS",value: ["Apple","Google"],},{name:"technology",type:"IS",value: ["Javascript","React"],},{name:"technology",type:"NOT",value: ["Vue","Angular"],},{name:"company",type:"NOT",value: ["Microsoft","Wipro"],},{name:"location",type:"NOT",value: ["Bangalore","Chennai"],},];
var output={};
for (var i=0;i<obj.length;i++){
	// console.log(obj[i]['name']);
	if(!(obj[i]['name'] in output)){
		output[obj[i]['name']]={};
	}
	if(obj[i]['type'] == "IS"){
		output[obj[i]['name']]['IS']= obj[i]['value'];
	}
	if(obj[i]['type'] == "NOT"){
		output[obj[i]['name']]['NOT']= obj[i]['value'];
	}
	// console.log(output);
}
console.log(output);
