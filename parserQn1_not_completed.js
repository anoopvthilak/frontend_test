var input1 = [
	{
		node: {
			name:"Elliot Alderson",
			age:"26",
			qualification:"Btech",
			jobTitle:"UI Developer",
			collegeEducation: {
				edges: [{
					node: {course:"Btech",
					collegeName:"MITS",
					year:"2017",
				},
			},]
			,},
			schoolEducation: {
				edges: [{
					node: {
						course:"High School",
						schoolName:"STPS",
						year:"2013",
					},
				},]
			,},
		},
	},
	{
		node: {
			name:"Darlene Alderson",
			age:"32",
			qualification:"MBA",
			jobTitle:"HR Manager",
			collegeEducation: {
				edges: [{
					node: {
						course:"MBA",
						collegeName:"IIM",
						year:"2018",
					},
				},]
			,},
			schoolEducation: {
				edges: [{
					node: {course:"High School",
					schoolName:"Nirmala High School",
					year:"2010",
				},
			},]
		,},
		},
	},
];
// console.log( input1);

var input2=[
{
	title: "Name",
	key: "name",
},
{
	title: "Age",
	key: "age",
},
{
	title: "Qualification",
	key: "qualification",
},
{
	title: "Job Title",
	key: "jobTitle",
},
{
	title: "College Education",
	key: "collegeEducation",
	children: [{
		title: "College",
		key: "education",
		children: [{
			title: "Course",
			key: "collegeEducation",
			dataKey: "course",
		},
		{
			title: "College Name",
			key: "collegeEducation",
			dataKey: "collegeName",
		},
		{
			title: " Year",
			key: "collegeEducation",
			dataKey: "year",
		},],
	},],
},
{
	title: "School Education",
	key: "schoolEducation",
	children: [{
		title: "School",
		key: "education",
		children: [{
			title: "Course",
			key: "schoolEducation",
			dataKey: "course",
		},
		{
			title: "School Name",
			key: "schoolEducation",
			dataKey: "schoolName",
		},
		{
			title: "Year",
			key: "schoolEducation",
			dataKey: "year",
	},
	],
	},
	],
},
]
// console.log(input2);


//Group2
var input3=[
{
node: {
orgName: "Apple Inc.",
tickerSymbol: "AAPL",
priceSet: {
edges: [
{
node: {
date: "Mar 26, 2021",
open: "120.35",
close: "121.21",
},
},
],
},

},
},
{
node: {
orgName: "Facebook, Inc",
tickerSymbol: "FB",
priceSet: {
edges: [
{
node: {
date: "Mar 26, 2021",
open: "278.30",
close: "283.02",
},
},
],
},
},
},
];

// console.log(input3);

var input4 = [
{
title: "Organization",
key: "orgName",
},
{
title: "Ticker Symbol",
key: "tickerSymbol",
},
{
title: "Price Set",
key: "priceSet",
children: [
{
title: "Price",
key: "price",
children: [
{
title: "Date",
key: "priceSet",

dataKey: "date",
},
{
title: "Open Price",
key: "priceSet",
dataKey: "open",
},
{
title: "Close Price",
key: "priceSet",
dataKey: "close",
},
],
},
],
},
];
// console.log(input4);
var output=[];
for(var i =0;i<input4.length;i++){
	for (var j= 0;j<input3.length;i++){

			var key_name = input4[i]['key'];
			if(input3[j]['node'][key_name]){
				output.push('{title : "name"}');
			
		}
	}
}